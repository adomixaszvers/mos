/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package os.processes;

import java.util.ArrayList;
import os.OS;
import os.Process;
import os.ProcessDescriptor;
import os.ProcessDescriptor.ProcessesName;
import os.Resource;
import os.ResourceDescriptor;
import os.resources.Message;
import os.ResourceDescriptor.ResourceName;

/**
 *
 * @author adomas
 */
public class StartStop extends Process {

    public StartStop(ProcessDescriptor pDesc, OS os) {
        super(pDesc, os);
    }

    @Override
    public void doStep() {
        switch (step) {
            case 0:
                os.createRes(ResourceName.Vartotojo_atmintis, new Message(null));
                os.createRes(ResourceName.Ivedimo_Irenginys, new Message(null));
                os.createRes(ResourceName.Isvedimo_Irenginys, new Message(null));
                os.createRes(ResourceName.Supervizorine_atm, new Message(null));
                step++;
                break;
            case 1:
                os.createdProc(ProcessesName.WaitForJob);
                os.createdProc(ProcessesName.Loader);
                os.createdProc(ProcessesName.MainProc);
                os.createdProc(ProcessesName.Interrupt);
                os.createdProc(ProcessesName.GetLine);
                os.createdProc(ProcessesName.PrintLine);
                step++;
                break;
            case 2:
                os.requestRes(ResourceName.MOS_Pabaiga);
                step++;
                break;
            case 3:
                for (Process process : pDesc.childrenProc) {
                    os.destroyProc(process);
                }
                step++;
                break;
            case 4:
                for (Resource resource : pDesc.createdRes) {
                    os.destroyRes(resource);
                }
                os.destroyProc(this);
                break;
        }
    }

}
