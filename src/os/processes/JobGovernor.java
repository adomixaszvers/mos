/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package os.processes;
import os.OS;
import os.Process;
import os.ProcessDescriptor;
import os.ResourceDescriptor;
import os.ResourceDescriptor.ResourceName;


/**
 *
 * @author adomas
 */
public class JobGovernor extends Process {
    
    public JobGovernor(ProcessDescriptor pDesc, OS os) {
        super(pDesc, os);
    }

    @Override
    public void doStep() {
        switch(step) {
            case 0:
                os.requestRes(ResourceName.Uzduotis_isroineje);
        }
    }
    
}
