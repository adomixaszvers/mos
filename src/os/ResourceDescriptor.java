/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package os;

import java.util.Queue;

/**
 *
 * @author LD
 */
public class ResourceDescriptor {

    public int intId;
    public ResourceName extId;
    public Process creatorProcess;
    public Resource element;
    public Process user;
    public Queue<Process> waitingProc;
    public boolean isReusable;

    public enum ResourceName {

        MOS_Pabaiga,
        Vartotojo_atmintis,
        Ivedimo_Irenginys,
        Isvedimo_Irenginys,
        Ivesta_Eilute,
        Eilute_Amintyje,
        Pranesimas_getLine_Procesui,
        Isvesta_Eilute,
        Pertraukimo_Pranesimas,
        Pertraukimas, // processor descr.

        Ivestas_Progr_Pav,
        Uzduotis_isroineje,
        Pranesimas_Loader, // is
        Eilute_Supervizor,
        Uzduotis_Vm, // is Loader (atmintyje)
        Supervizorine_atm
    }

    public ResourceDescriptor(int intId, ResourceName extId, Process creatorProcess, Resource element,boolean isReusable) {
        this.intId = intId;
        this.extId = extId;
        this.creatorProcess = creatorProcess;
        this.element = element;
        this.isReusable = isReusable;
    }

    
}
