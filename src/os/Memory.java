package os;

import java.util.ArrayList;
import utils.CastException;
import static utils.Utils.byteToInt;
import static utils.Utils.charToInt;
import static java.util.Collections.shuffle;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author LD
 */
public class Memory {

    public final static int WORD_SIZE = 4;
    public final static int BLOCK_SIZE = 10;
    public final static int BLOCKS = 70;
    public final static int USER_MEMORY_BLOCKS = 50;
    public final static int PAGE_TABLE_BLOCKS = 10;
    public final byte[] memory;
    public OS os;

    public Memory(OS os) {
        this.os = os;
        this.memory = new byte[WORD_SIZE * BLOCK_SIZE * BLOCKS];
    }
    
    public int realAddress(char x, char y) throws CastException {
        int block = byteToInt(os.processor.PLR[2]) * 10 + byteToInt(os.processor.PLR[3]);
        int a2 = memory[block + charToInt(x) * WORD_SIZE + 3];
        return a2 * 10 + charToInt(y);
    }
    
}
