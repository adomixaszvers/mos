/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package os.resources;

import os.Resource;
import os.ResourceDescriptor;

/**
 *
 * @author Adomas
 */
import static os.Memory.*;

public class IOResource extends Resource{
    public byte[] bytes;

    public IOResource(ResourceDescriptor rDesc) {
        super(rDesc);
        this.bytes = new byte[BLOCK_SIZE*WORD_SIZE*10];
    }
    
}
