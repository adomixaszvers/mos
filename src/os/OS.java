/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package os;

import java.util.EnumMap;
import java.util.PriorityQueue;
import java.util.Set;
import os.ProcessDescriptor.ProcessesName;
import os.ProcessDescriptor.ProcessesState;
import os.ResourceDescriptor.ResourceName;
import os.processes.GetLine;
import os.processes.Interrupt;
import os.processes.JobGovernor;
import os.processes.Loader;
import os.processes.MainProc;
import os.processes.PrintLine;
import os.processes.StartStop;
import os.processes.VirtualMachine;
import os.processes.WaitForJob;
import os.resources.Message;

/**
 *
 * @author adomas
 */
public class OS {

    public Processor processor;
    public Memory memory;

    public Process runningProc;
    public PriorityQueue<Process> readyProc;
    public PriorityQueue<Process> blockedProc;
    public PriorityQueue<Process> waitingProc;
    public Set <Resource> readyResource;    
    
    private int newProcID=0, newResID=0;

    public void createdProc(ProcessesName extID) {
        Process newProc;
        ProcessDescriptor newPDesc;
        switch (extID) {
            case StartStop:
                newPDesc = new ProcessDescriptor(generateNewProcID(), extID, null, 100);
                newProc = new StartStop(newPDesc, this);
                break;
            case WaitForJob:
                newPDesc = new ProcessDescriptor(generateNewProcID(), extID, runningProc, 90);
                newProc = new WaitForJob(newPDesc, this);
                break;
            case MainProc:
                newPDesc = new ProcessDescriptor(generateNewProcID(), extID, runningProc, 90);
                newProc = new MainProc(newPDesc, this);
                break;
            case Loader:
                newPDesc = new ProcessDescriptor(generateNewProcID(), extID, runningProc, 90);
                newProc = new Loader(newPDesc, this);
                break;
            case JobGovernor:
                newPDesc = new ProcessDescriptor(generateNewProcID(), extID, runningProc, 80);
                newProc = new JobGovernor(newPDesc, this);
                break;
            case VirtualMachine:
                newPDesc = new ProcessDescriptor(generateNewProcID(), extID, runningProc, 0);
                newProc = new VirtualMachine(newPDesc, this);
                break;
            case Interrupt:
                newPDesc = new ProcessDescriptor(generateNewProcID(), extID, runningProc, 90);
                newProc = new Interrupt(newPDesc, this);
                break;
            case PrintLine:
                newPDesc = new ProcessDescriptor(generateNewProcID(), extID, runningProc, 90);
                newProc = new PrintLine(newPDesc, this);
                break;
            case GetLine:
                newPDesc = new ProcessDescriptor(generateNewProcID(), extID, runningProc, 90);
                newProc = new GetLine(newPDesc, this);
                break;
            default:
                newProc = null;
        }

        if (newProc == null) {
            return;
        }

        runningProc.pDesc.childrenProc.add(newProc.updateDate());
        processPlanner();
    }

    public void destroyProc(Process process) {
        destroyProc(process, runningProc);
    }

    private void destroyProc(Process process, Process caller) {
        for(Resource resource: process.pDesc.createdRes) {
            destroyRes(resource);
        }
        for(Resource resource: process.pDesc.ownedRes) {
            releaseRes(resource);
        }
        if (caller.pDesc.childrenProc.contains(process) || caller.pDesc.extID == ProcessesName.StartStop) {
            for (Process child : runningProc.pDesc.childrenProc) {
                destroyProc(child, process);
            }
            switch(process.pDesc.pState) {
                case READY:
                case SUSPENDED_READY:
                    this.readyProc.remove(process);
                    break;
                case BLOCKED:
                case SUSPENDED_BLOCKED:
                    this.blockedProc.remove(process);
                    this.waitingProc.remove(process);
                    break;
            }
        }
    }

    public void activateProc(Process process) {
        if (process.pDesc.pState == ProcessesState.SUSPENDED_READY) {
            process.pDesc.pState = ProcessesState.READY;
            this.blockedProc.remove(process);
            this.readyProc.add(process.updateDate());
        } else if (process.pDesc.pState == ProcessesState.SUSPENDED_BLOCKED) {
            process.pDesc.pState = ProcessesState.BLOCKED;
            this.blockedProc.remove(process);
            this.readyProc.add(process.updateDate());
        }
        processPlanner();
    }

    public void suspendProc(Process process) {
        if (process.pDesc.pState == ProcessesState.READY) {
            process.pDesc.pState = ProcessesState.SUSPENDED_READY;
            this.readyProc.remove(process);
            this.blockedProc.add(process);
        } else if (process.pDesc.pState == ProcessesState.BLOCKED) {
            process.pDesc.pState = ProcessesState.SUSPENDED_BLOCKED;
            this.readyProc.remove(process);
            this.blockedProc.add(process);
        }
    }
    
    public void processPlanner() {
        if(runningProc.pDesc.pState == ProcessesState.BLOCKED) {
            runningProc.pDesc.savedState.loadStateFrom(processor);
            blockedProc.add(runningProc);
        }
        if(!readyProc.isEmpty()) {
            runningProc = readyProc.remove();
            runningProc.pDesc.pState = ProcessesState.RUNNING;
            runningProc.pDesc.savedState.saveStateTo(processor);
        } else {
            createRes(ResourceName.MOS_Pabaiga, new Message(null));
        }
    }
    
    public int generateNewProcID() {
        newProcID++;
        return newProcID;
    }
    
    public int generateNewResID() {
        newResID++;
        return newResID;
    }
    
    public void createRes(ResourceName extID, Resource element) {
        ResourceDescriptor newResD = new ResourceDescriptor(generateNewResID(), extID, runningProc, element, false);
        element.rDesc = newResD;
        readyResource.add(element);
        runningProc.pDesc.createdRes.add(element);
        resourcePlanner();
    }
    
    public void destroyRes(Resource element) {
        if(element.rDesc.user != null) {
            readyResource.remove(element);
        } else {
            Process user = element.rDesc.user;
            user.pDesc.ownedRes.remove(element);
        }
        Process process = element.rDesc.creatorProcess;
        if(process != null)
            process.pDesc.createdRes.remove(element);
    }
    
    public void requestRes(ResourceName extID) {
        for (Resource resource: readyResource) {
            if (resource.rDesc.extId == extID){
                if(resource.rDesc.user == null) {
                    resource.rDesc.user = runningProc;
                    runningProc.pDesc.ownedRes.add(resource);
                    readyResource.remove(resource);
                } 
                return;
            }
        }
        runningProc.pDesc.waintingFor = extID;
        waitingProc.add(runningProc);
        runningProc.pDesc.pState = ProcessesState.BLOCKED;
    }
    
    public void releaseRes(Resource resource) {
        resource.rDesc.user = null;
        runningProc.pDesc.ownedRes.remove(resource);
        if(resource.rDesc.isReusable)
            readyResource.add(resource);
        resourcePlanner();
    }
    
    public void resourcePlanner() {
        for (Process process: waitingProc) {
            outerloop:
            for(Resource resource: readyResource) {
                if(process.pDesc.waintingFor == resource.rDesc.extId) {
                    resource.rDesc.waitingProc.add(process);
                    waitingProc.remove(process);
                    blockedProc.add(process);
                    continue outerloop;
                }
            }
        }
        for(Resource resource: readyResource) {
            if(resource.rDesc.waitingProc.isEmpty())
                continue;
            Process process = resource.rDesc.waitingProc.remove();
            readyResource.remove(resource);
            process.pDesc.ownedRes.add(resource);
            resource.rDesc.user = process;
            if (process.pDesc.pState== ProcessesState.BLOCKED){
                process.pDesc.pState = ProcessesState.READY;
                blockedProc.remove(process);
                readyProc.add(process.updateDate());
            } else {
                process.pDesc.pState = ProcessesState.SUSPENDED_READY;
            }
        }
    }
}
