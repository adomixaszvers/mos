/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package os;

import java.util.Date;

/**
 *
 * @author adomas
 */
public abstract class Process implements Comparable<Process> {

    public ProcessDescriptor pDesc;
    public OS os;
    private Date date;
    public int step=0;
    
    public Process(ProcessDescriptor pDesc, OS os) {
        this.pDesc = pDesc;
        this.os = os;
        this.date = new Date();
    }

    @Override
    public int compareTo(Process o) {
        if(this.pDesc.priority != o.pDesc.priority)
            return this.pDesc.priority - o.pDesc.priority;
        else return this.date.compareTo(o.date);
    }
    public Process updateDate() {
        this.date = new Date();
        return this;
    }

    public abstract void doStep();
    public Resource getResource(ResourceDescriptor.ResourceName extID) {
        Resource requestedResource=null;
        for(Resource resource: pDesc.ownedRes) {
            if(extID == resource.rDesc.extId) {
                requestedResource = resource;
                break;
            }
        }
        return requestedResource;
    }
}
